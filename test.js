const axios = require('axios');
const url = 'http://localhost:3000';

const startTest = () => {
  setInterval(function() {
    getPressureData();
  },1000)
}

const getPressureData = () => {
  axios.get(url + '/getPressureEvent')
        .then(function (response) {
          console.log(response.data);
        })
        .catch(function (error) {
          console.log('Error: ' + error.code);
        });
}

startTest();
